# Task 0

Clone this repository (well done!)

# Task 1

Take a look a the two repositories:
  
  * (A) https://bitbucket.org/farleyknight/ruby-git
  * (B) https://bitbucket.org/kennethendfinger/git-repo

And answer the following questions about them:

  * Who made the last commit to repository A?
  farleyknight
  
  * Who made the first commit to repository A?
  scott Chacon
  
  * Who made the last and first commits to repository B?
  Shawn O. Pearce(last)
  The Android Open(first)
  
  * Who has been the most active recent contributor on repository A?  How about repository B?
  Joshua Nichols(A)
  Shawn O. Pearce(B)
  
  * Are either/both of these projects active at the moment?  🤔 If not, what do you think happened?
  No, A has finished in 12-2012, B has finished 3-2012
  
  * 🤔 Which file in each project has had the most activity?
  lib(A)
  subcmds(B)

# Task 2

Setup a new eclipse project with a main method that will print the following message to the console when run:

~~~~~
Sheep and Wolves
~~~~~ 

🤔 Now setup a new bitbucket repository and have this project pushed to that repository. 
 You will first need to `commit`, then `push`.  Ensure you have setup an appropriate `.gitignore` file. 
 The one we have in this repository is a very good start.